#include <iostream>
#include <iomanip>
#include <SFML/Graphics.hpp>
#include "ball.hpp"
#include "game.hpp"
#include "paddle.hpp"

Ball::Ball(double radius) : _radius(radius)
{
    _shape = new sf::CircleShape(radius);
}

Ball::~Ball()
{
    delete _shape;
}

void Ball::update(Game* game, sf::Time elapsed)
{
    sf::Int32 elapsed_msec = elapsed.asMicroseconds();
    //std::cout << "elapsed: " << std::setprecision(15) << elapsed_msec << std::endl;

    double new_x = _x + _velocity_x * elapsed_msec * 0.000001;
    double new_y = _y + _velocity_y * elapsed_msec * 0.000001;

    //std::cout << "new_pos=" << new_x << ", " << new_y << std::endl;

    set_position(new_x, new_y);
}

void Ball::draw(sf::RenderWindow* window)
{
    window->draw(*_shape);
}

void Ball::set_position(double x, double y)
{
    _x = x;
    _y = y;

    _shape->setPosition(x, y);
}

double Ball::get_x()
{
    return _x;
}

double Ball::get_y()
{
    return _y;
}

void Ball::set_velocity(double x, double y)
{
    _velocity_x = x;
    _velocity_y = y;
}

double Ball::get_velocity_x()
{
    return _velocity_x;
}

double Ball::get_velocity_y()
{
    return _velocity_y;
}