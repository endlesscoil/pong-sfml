#ifndef __PADDLE_HPP
#define __PADDLE_HPP

#include <SFML/Graphics/Rect.hpp>

namespace sf 
{
    class RectangleShape;
    class RenderWindow;
}

class Paddle
{
    private:
        int _width = -1;
        int _height = -1;
        double _x = 0;
        double _y = 0;
        sf::RectangleShape* _shape = nullptr;

    public:
        Paddle() = delete;
        Paddle(int width, int height);
        ~Paddle();

        void draw(sf::RenderWindow* window);
        void move(double value);

        void set_position(double x, double y);
        double get_x();
        double get_y();

        int get_width();
        int get_height();
        sf::Rect<float> get_rect();

    private:

};

#endif /* __PADDLE_HPP */