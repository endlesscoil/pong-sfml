#include <iostream>
#include <SFML/Graphics.hpp>

#include <game.hpp>

void renderLoop(Game* game)
{
    game->render();
}

int main(int argc, char** argv)
{
    Game game;

    sf::Thread thread(&renderLoop, &game);
    thread.launch();
    
    game.loop();

    std::cout << "done" << std::endl;

    return 0;
}
