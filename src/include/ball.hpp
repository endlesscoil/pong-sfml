#ifndef __BALL_HPP
#define __BALL_HPP

namespace sf
{
    class CircleShape;
    class RenderWindow;
    class Time;
}

class Game;

class Ball
{
public:
    const static int DEFAULT_RADIUS = 5;

private:
    double _radius = 0;
    double _x = 0;
    double _y = 0;
    double _velocity_x = 0;
    double _velocity_y = 0;
    sf::CircleShape* _shape = nullptr;

public:
    Ball() : Ball(DEFAULT_RADIUS) { };
    Ball(double radius);
    ~Ball();

    void update(Game* game, sf::Time elapsed);
    void draw(sf::RenderWindow* window);

    void set_position(double x, double y);
    double get_x();
    double get_y();

    void set_velocity(double x, double y);
    double get_velocity_x();
    double get_velocity_y();

    inline double get_radius() { return _radius; };
};

#endif /* __BALL_HPP */