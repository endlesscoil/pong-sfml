#include <SFML/Graphics.hpp>
#include "paddle.hpp"

Paddle::Paddle(int width, int height) : _width(width), _height(height)
{
    _shape = new sf::RectangleShape(sf::Vector2f(width, height));
}

Paddle::~Paddle()
{
    delete _shape;
}

void Paddle::draw(sf::RenderWindow* window)
{
    window->draw(*_shape);
}

void Paddle::move(double value)
{
    set_position(_x, _y + value);
}

void Paddle::set_position(double x, double y)
{
    _x = x;
    _y = y;

    _shape->setPosition(x, y);
}

double Paddle::get_x()
{
    return _x;
}

double Paddle::get_y()
{
    return _y;
}

int Paddle::get_width()
{
    return _width;
}

int Paddle::get_height()
{
    return _height;
}

sf::Rect<float> Paddle::get_rect()
{
    return sf::Rect<float>({x: _x, y: _y}, {width: _width, height: _height});
}