solution "Pong-SFML"
    configurations { "Debug", "Release" }
    
    project "Pong-SFML"
        kind "ConsoleApp"
        language "C++"
        files { "**.hpp", "**.cpp" }
        includedirs { "src/include" }

        configuration "Debug"
            defines { "DEBUG" }
            flags { "Symbols" }
    
        configuration "Release"
            defines { "NDEBUG" }
            flags { "Optimize" }

        configuration { "linux", "gmake" }
            buildoptions { "-std=c++0x" }
            links { "sfml-graphics", "sfml-window", "sfml-system" }