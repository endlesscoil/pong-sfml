#include <iostream>
#include <iomanip>
#include <string>

#include <stdlib.h>
#include <time.h>

#include <SFML/Graphics.hpp>
#include "game.hpp"
#include "paddle.hpp"
#include "ball.hpp"

Game::Game(int width, int height) : _width(width), _height(height)
{
    srand(time(NULL));

    init_graphics();
}

Game::~Game()
{
    for (auto box: _score_boxes)
        delete box;

    for (auto p: _paddles)
        delete p;

    delete _ball;
    delete _font;
    delete _window;
}

void Game::init_graphics()
{
    _window = new sf::RenderWindow(sf::VideoMode(_width, _height), "SFML works!");
    _window->setFramerateLimit(60);
    _window->setActive(false);

    _scores.push_back(0);
    _scores.push_back(0);

    _font = new sf::Font();
    _font->loadFromFile("arial.ttf");

    for (int i = 0; i < 2; ++i)
    {
        sf::Text* text = new sf::Text();

        text->setFont(*_font);
        text->setCharacterSize(24);
        text->setColor(sf::Color::White);

        int text_pos = 0;
        if (i == 1)
            text_pos = _width - 24*8;
        text->setPosition(text_pos, 0);

        _score_boxes.push_back(text);
    }

    _paddles.push_back(new Paddle(15, 100));
    _paddles[0]->set_position(_paddles[0]->get_width(), 100);
    _paddles.push_back(new Paddle(15, 100));
    _paddles[1]->set_position(_width - _paddles[1]->get_width() * 2, 100);
}

void Game::loop()
{
    sf::Clock clock;

    while (_window->isOpen())
    {
        update(&clock);
    }
}

void Game::update(sf::Clock* clock)
{
    if (!_ball || clock->getElapsedTime().asMicroseconds() < 1)
        return;

    sf::Time elapsed = clock->restart();

    update_ball(elapsed);
}

void Game::update_ball(sf::Time elapsed)
{
    _ball->update(this, elapsed);

    double ball_x = _ball->get_x();
    double ball_y = _ball->get_y();
    double vel_x = _ball->get_velocity_x();
    double vel_y = _ball->get_velocity_y();

    if (ball_y < 0 || ball_y > _height)
    {
        ball_y = ball_y > _height ? _height : 0;
        vel_y = -vel_y;
    }

    sf::Rect<float> myrect({x: ball_x, y: ball_y}, {width: _ball->get_radius(), height: _ball->get_radius()});
    if (myrect.intersects(_paddles[0]->get_rect()) || myrect.intersects(_paddles[1]->get_rect()))
    {
        vel_x = -vel_x;
    }

/*    std::cout << "ball=" << ball_x << ", " << ball_y << std::endl;
    std::cout << "vel=" << vel_x << ", " << vel_y << std::endl;*/

    _ball->set_position(ball_x, ball_y);
    _ball->set_velocity(vel_x, vel_y);

    if (ball_x < 0 || ball_x > 800)
    {
        serve_ball();

        _scores[ball_x < 0 ? 1 : 0] += 1;
    }
}

void Game::render()
{
    sf::Clock clock;

    while (_window->isOpen())
    {
        sf::Event event;

        while(_window->pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                _window->close();

            else if (event.type == sf::Event::KeyPressed)
                handle_keypress(event);
        }

        sf::Time elapsed = clock.restart();
        handle_held_keys(elapsed);

        _window->clear();

        if (_ball)
            _ball->draw(_window);

        for (auto p: _paddles)
            p->draw(_window);

        draw_score();

        _window->display(); 
    }
}

void Game::serve_ball()
{
    delete _ball;
    _ball = nullptr;

    _ball = new Ball(5);
    _ball->set_position(_width / 2 - 5 / 2, _height / 2 - 5 / 2);

    int direction = rand() % 360;
    int vel = (rand() % 500) + 200;
    int new_x = 0 * cos(direction) - vel * sin(direction);
    int new_y = 0 * sin(direction) + vel * cos(direction);

    if (new_x == 0)
        new_x = 1;

    //std::cout << "serving ball with vel=" << vel << std::endl;

    _ball->set_velocity(new_x, new_y);
}

void Game::draw_score()
{
    _score_boxes[0]->setString("Player 1: " + std::to_string(_scores[0]));
    _score_boxes[1]->setString("Player 2: " + std::to_string(_scores[1]));

    _window->draw(*_score_boxes[0]);
    _window->draw(*_score_boxes[1]);
}

Paddle* Game::get_paddle(int idx)
{
    return _paddles[idx];
}

void Game::handle_held_keys(sf::Time elapsed)
{
    sf::Int32 elapsed_msec = elapsed.asMicroseconds();
    //std::cout << "elapsed: " << std::setprecision(15) << elapsed_msec << std::endl;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
        _paddles[0]->move(-30 * elapsed_msec * 0.00001);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        _paddles[0]->move(30 * elapsed_msec * 0.00001);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        _paddles[1]->move(-30 * elapsed_msec * 0.00001);
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
        _paddles[1]->move(30 * elapsed_msec * 0.00001);
}

void Game::handle_keypress(sf::Event& event)
{
    if (event.key.code == sf::Keyboard::Escape)
    {
        _window->close();
    }

    if (event.key.code == sf::Keyboard::Space)
    {
        serve_ball();
    }
}