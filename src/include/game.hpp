#ifndef __GAME_HPP
#define __GAME_HPP

#include <vector>

const static int DEFAULT_WIDTH = 800;
const static int DEFAULT_HEIGHT = 600;

namespace sf 
{
    class RenderWindow;
    class CircleShape;
    class Event;
    class Clock;
    class Time;
    class Text;
    class Font;
}

class Paddle;
class Ball;

class Game
{
private:
    int _width = DEFAULT_WIDTH;
    int _height = DEFAULT_HEIGHT;

    sf::RenderWindow* _window = nullptr;
    std::vector<Paddle*> _paddles;
    std::vector<int> _scores;
    Ball* _ball = nullptr;
    sf::Font* _font = nullptr;
    std::vector<sf::Text*> _score_boxes;

public:
    Game() : Game(DEFAULT_WIDTH, DEFAULT_HEIGHT) { };
    Game(int width, int height);
    ~Game();

    void loop();
    void update(sf::Clock* clock);
    void update_ball(sf::Time elapsed);
    void render();

    void serve_ball();
    void draw_score();

    Paddle* get_paddle(int idx);
    inline int get_width() { return _width; };
    inline int get_height() { return _height; };

private:    
    void init_graphics();
    void handle_keypress(sf::Event& event);
    void handle_held_keys(sf::Time);

};

#endif /* __GAME_HPP */